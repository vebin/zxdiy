Component({
    properties: {
        props: String,
        relation: Array
    },
    data: {
        productConfig: {
            cart_icon: 1,
            cart_icon_color: "#fc4c4f",
            label_icon: 0,
            label_icon_color: "#fc4c4f",
            label_name: "新品",
            product_type: 0,
            products: [],
            show_type: 0
        },
        productData: [{
            relation: {
                detail: "<p>商品详情</p>",
                fee_price: 0,
                fee_type: 0,
                id: 0,
                img_url: "",
                info: "商品简介",
                name: "商品名称",
                price: 0,
                price_org: 0,
                rank: 0,
                sale: 0,
                shop_id: 0,
                sku_status: 0,
                status: 1,
                subname: null,
                unit: "件"
            }
        }],
        color: wx.getExtConfigSync ? wx.getExtConfigSync().color : {}
    },
    attached: function () {
        // console.log(this);
        if (Object.keys(this.data.props).length) {
            this.setData({
                productConfig: Object.assign(this.data.productConfig, this.data.props ? JSON.parse(this.data.props) : {})
            })
        }
        if (this.data.relation.length) {
            this.setData({
                productData: this.data.relation
            })
        }
    },
    methods: {
        goProductDetail(e) {
            wx.navigateTo({
                url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.item.id}`
            });
        }
    }
});